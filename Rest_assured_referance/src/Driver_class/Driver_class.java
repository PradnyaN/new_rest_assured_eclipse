package Driver_class;

import java.io.IOException;

import Test_package.Put_tc1;
import Test_package.patch_tc1;
import Test_package.post_tc1;

public class Driver_class {

	public static void main(String[] args) throws IOException {

		post_tc1.executor();
		Put_tc1.executor();
		patch_tc1.executor();
	}

}
