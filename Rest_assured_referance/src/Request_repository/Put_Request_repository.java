package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_Method.New_excel;

public class Put_Request_repository {
	public static String put_tc1() throws IOException {
		ArrayList<String> Data = New_excel.Excel_Read_data("Testdata", "Put_API", "Put_tc 1");
		System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);

//String requestbody="{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";

		return requestbody;
	}

}
