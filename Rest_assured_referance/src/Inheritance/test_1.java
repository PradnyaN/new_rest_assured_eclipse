package Inheritance;



class P

{
	int a;
	int b;
	public void display()
	{
	System.out.println(a+b);
	}
	
}


class V extends P 
{
	int y;
	int z;
	public void show()
	{
	System.out.println(y+z);
	}
}

class D extends V
{
	int f;
	int g;
	public void adding()
	{
		System.out.println(f+g);
	}
	
}

public class test_1 {
	

	public static void main(String[] args) {
	P aobject= new P();
	aobject.a=10;
	aobject.b=20;
	aobject.display();
	
	V bobject= new V();
	bobject.y=30;
	bobject.z=40;
	bobject.show();
	
	bobject.a=600;
	bobject.b=600;
	bobject.display();
	
	D dobject= new D();
	dobject.a=500;
	dobject.b=400;
	dobject.f=300;
	dobject.g=200;
	dobject.y=100;
	dobject.z=600;
	
	dobject.adding();
	dobject.display();
	dobject.show();
	}

}
