package Restassured_reference;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class post_reference {

	public static <Jsonpath> void main(String[] args) {

// step 1 declare base URL
		RestAssured.baseURI = "https://reqres.in/";
//		step 2 configure the request parameters and trigger the API

		String responsebody = given().header("Content-Type", "application/json")
				.body("{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}").log().all()
				.when().post("api/users")
				.then().log().all().extract().response().asString();
		
		//System.out.println("responseBody is :" + responseBody);
// step 3 create and object of JSON path to parse the responseBody
		JsonPath jsp_res=new JsonPath(responsebody);
		String res_name=jsp_res.getString("name");   
		String res_id=jsp_res.getString("id");
		String res_job=jsp_res.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expectedDate = currentdate.toString().substring(0,11);
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0,11);
// Step 4 Validate responseBody
		Assert.assertEquals(res_name,"morpheus");
		Assert.assertEquals(res_job,"leader");
		Assert.assertEquals(res_id,"111");
		Assert.assertEquals(res_createdate, expectedDate);
	}

}
