package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_Common_Method.Common_method_handle_API;
import Endpoints.Put_endpoint_testcase1;
import Request_repository.Put_Request_repository;
import Utility_Common_Method.Handle_API_Logs;
import Utility_Common_Method.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class Put_tc1 extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir = Handle_Directory.create_log_directory("Put_tc1_logs");
		String requestBody = Put_Request_repository.put_tc1();
		String endpoint = Put_endpoint_testcase1.Put_endpoint_tc1();
		for (int i = 0; i < 3; i++) {
			int statusCode = put_statuscode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				String responseBody = put_responsebody1(requestBody, endpoint);
				System.out.println(responseBody);
				Handle_API_Logs.evidence_creator(log_dir, "Put_tc1", endpoint, requestBody, responseBody);
				Put_tc1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

}
