package Utility_Common_Method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static void main(String[] args) throws IOException {
		String project_dir = System.getProperty("user.dir");

//Step 1 Create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Data_Files\\Testdata.xlsx");

//Step 2 Create the XSSFWorkbook to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

//step 3 fetch the no of sheets available in the excel file 
		int count = wb.getNumberOfSheets();

//Step 4 access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetname = wb.getSheetName(i);
			System.out.println(sheetname);
			if (sheetname.equals("Post_API")) {
				System.out.println(sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String tcname = datarow.getCell(0).getStringCellValue();
					if (tcname.equals("Post_tc 2")) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String Testdata = cellvalues.next().getStringCellValue();
							System.out.println(Testdata);

						}
					}
				}
				break;
			}
		}
	}
}
