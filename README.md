Rest Assured Framework

> Test Driving Mechanism(Driver Class)

> Test Script

> Common Functions (API Related & Utility Related)

> Data & variable File

> Liabraries

> Reports


In my Framework i am using Static Driver Class and Dynamic Driver Class for drive Test Execution.
In Static Driver Class i am importing all test scripts and calling main method for execution and in Dynamic Driver class i am using "Java.lang.reflect" for dynamically call Test scripts from my test script package and execute it..

Test Script Package :Each API we have multiple test cases and each test case we have single test script. All test scripts which we need to created.

Common Functions: 1. API Related Common Functions
                  2. Utility Related Common Functions
 > API Related Common Function: In this Function we are going to trigger the API and extract responsebody and Statuscode .

 > Utility Related common Function: This function is used to creating API logs into text Files once the API is executed then it is co-related with all endpoints,requestbody,responsebody which can be stored in notepad.Another utility to read the data from an excel file.

Utility : This is for create the directory for test execution automatically.If it is dosent exist in the project and if it is exist then we can delete old one and create the new one for log files.

Data/Variable File : In this function we can read the data from an excel file by using "Apache POI Liabrary".

Liabrary : In our Framework we are using Rest Assured to Trigger the API ,Extract statuscode,responsebody and parse the responsebody by using "JSON Path" . And for validating responsebody we are using TestNG liabraby by using assert.It is managed by maven repository.

Reports : For Report generation we are using Extent Reports and Allure Reports.

