package Driver_class;

import API_Common_Method.Common_method_handle_API;

public class New_Driver extends Common_method_handle_API{

	public static void main(String[] args) {
		String post_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String post_endpoint = "https://reqres.in/api/users";
		int statuscode = post_statuscode(post_requestBody, post_endpoint);
		System.out.println(statuscode);
		
		String post_responseBody = post_responsebody(post_requestBody, post_endpoint);
		System.out.println(post_responseBody);

		String put_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String put_endpoint = "https://reqres.in/api/users/2";
		int put_statuscode = put_statuscode(put_requestBody, put_endpoint);
	    System.out.println(put_statuscode);
	    
	    String put_responseBody = put_responsebody1(put_requestBody, put_endpoint);
	    System.out.println(put_responseBody);
		

		String patch_requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		String patch_endpoint = "https://reqres.in/api/users/2";
		int patch_statuscode = patch_statuscode(patch_requestBody,patch_endpoint);
		System.out.println(patch_statuscode);
		
		String patch_responseBody = patch_responsebody(patch_requestBody, patch_endpoint);
		System.out.println(patch_responseBody);
		
		
		String get_endpoint = "https://reqres.in/api/users?page=1";
		int get_statuscode = get_statuscode(get_endpoint);
		System.out.println(get_statuscode);
		
		String get_responseBody = get_responsebody(get_endpoint);
		System.out.println(get_responseBody);
	}

}
