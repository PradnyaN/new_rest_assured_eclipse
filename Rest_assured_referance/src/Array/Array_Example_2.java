package Array;

public class Array_Example_2 {

	public static void main(String[] args) {
// find the array length without using length function 
int inpArray[] = { 3, 10, 11, 2 };

	//method 1 :
		int len = 0;
		for (int i : inpArray) {
			len++;
		}
		System.out.println("the length of array is : " + len);

		//method 2 :
		int count = inpArray.length;
		int len1 = 0;
	for (int i = 0; i < count; i++) {
			len1++;
		}
	System.out.println("the length of array is : " + len1);

// Reverse a String without using reverse method 
	String inpStr = "Java";

		int len14 = inpStr.length();
		for (int i = len14; i > 0; i--) {
			System.out.print(inpStr.charAt(i - 1));
		}

// Reverse a given array without using reverse method 
		int inpArr[] = { 3, 10, 11, 2 };
		int len12 = inpArr.length;

		for (int i = len12; i > 0; i--) {
			System.out.print(inpArr[i - 1] + " ");
		}

// Find the maximum number from given array 
		int arrList[] = { 3, 10, 5, 11, 2, 15, 20, 13, 100 };
		int len10 = arrList.length;
		int a = 0;

		for (int i = 0; i < len10; i++) {
			if (a < arrList[i]) {
				a = arrList[i];
				System.out.println(a);
			}
		}
		System.out.println(a);

// Reverse a Statement without using reverse method 
		String inpStr2 = "Java is best Programming Language";
		String[] a1 = inpStr2.split(" ");
		int len11 = a1.length;
		for (int i = len11; i > 0; i--) {
			System.out.print(a1[i - 1] + " ");
		}
	}

}
