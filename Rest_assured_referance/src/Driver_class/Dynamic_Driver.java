package Driver_class;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Utility_Common_Method.New_excel;

public class Dynamic_Driver {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_execute = New_excel.Excel_Read_data("Testdata", "Testcases", "TCName");
		System.out.println(TC_execute);
		int count = TC_execute.size();
		for (int i = 1; i < count; i++) {
			String TCname = TC_execute.get(i);
			System.out.println(TCname);
			// call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> Test_class = Class.forName("Test_package." + TCname);
			
			// call the execute method belonging to test class captured in variable TC_name
			// by using java.lang.reflect.method class
			Method execute_method = Test_class.getDeclaredMethod("executor");
			// set the accessibility of method true
			execute_method.setAccessible(true);
			// create the instance of testclass captured in variable name testclassname
			Object instance_of_test_class = Test_class.getDeclaredConstructor().newInstance();
			// execute the test script class fetch in the varialble test_class
			execute_method.invoke(instance_of_test_class);

		}
	}

}
