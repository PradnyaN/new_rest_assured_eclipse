package Operators;

public class Operators_Java {

	public static void main(String[] args) {
// Arithmetic Operators	
 int h=20;
 int k=40;
 System.out.println(h+k);
 System.out.println(h-k);
 System.out.println(h*k);
 System.out.println(h/k);
 System.out.println(h%k);

// Relational Operators
 System.out.println(h<k);//True
 System.out.println(h>k);//False
 System.out.println(h<=k);//True
 System.out.println(h>=k);//False
 System.out.println(h==k);//False
 System.out.println(h!=k);//True
 
 
// Logical Operators
 boolean  x = true;
 boolean  y = false;
 System.out.println(x && y);//False
 System.out.println(x || y);//True
 System.out.println( !x );//False
 System.out.println( !y );//True
 
 // Assignment Operators
 int aa;
 aa=h;
 System.out.println(aa);
 aa=k;
 System.out.println(aa);
 
 aa=100;
 aa=aa+1;
 System.out.println(aa);
 
 // Increment Operators
 aa++;     //aa=aa+1;
 System.out.println(aa);
 aa+=6;   //aa=aa+6; 
 
// Decrement Operators
 aa--;    //aa=aa-1;
 System.out.println(aa);
 aa-=6;   //aa=aa-6
	}
}
